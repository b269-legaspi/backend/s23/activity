const trainer = {
  name: "Ashhh Ketchum",
  age: 25,
  pokemon: ["pikachu", "charizard", "squirtle", "bulbasaur"],
  friends: { hoenn: ["May", "Max"], kanto: ["Brock", "Misty"] },
};

trainer.talk = function () {
  console.log("Pikachu! I choose you!");
};

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

trainer.talk();

const Pokemon = function (name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 3;
  this.attack = level;
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name);
    target.health -= this.attack;
    console.log(target.name + "'s health is now reduced to " + target.health);
    if (target.health <= 0) {
      target.faint();
    }
  };
  this.faint = function () {
    console.log(this.name + " has fainted.");
  };
};

const pikachu = new Pokemon("pikachu", "10");
const geodude = new Pokemon("geodude", "8");
const squirtle = new Pokemon("squirtle", "1");

console.log(pikachu);
console.log(geodude);

console.log(geodude.name, geodude.health);
pikachu.tackle(geodude);
pikachu.tackle(geodude);
pikachu.tackle(geodude);
console.log(squirtle.name, squirtle.health);
pikachu.tackle(squirtle);
